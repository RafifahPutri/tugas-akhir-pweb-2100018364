<!DOCTYPE html>
<html>
<head>
  <title>Daftar</title>
  <link rel="stylesheet" type="text/css" href="responsipweba.css">
  <script src="https://kit.fontawesome.com/c4254e24a8.js" crossorigin="anonymous"></script>
</head>
<body background="gambar1.jpg">
  <div class="header">
      <br><strong><span class="type"></span></strong><br></div><br>
    <div class="container">
      <form action="proses.php"method="post" name="input">
        <h3><font face="algerian">BIODATA PRIBADI</h3></font>
      <div class="input-control">
        <label>Nama Lengkap</label>
        <input type="text" name="namaa" id="namaa" onkeyup="validateNamea()">
        <span id="namaa-error"></span>
      </div>
        <div class="input-control">
        <label>Nama Panggilan</label>
        <input type="text" name="nama" id="nama" onkeyup="validateName()">
        <span id="nama-error"></span>
      </div>
      <div class= "input-control">
          <label>Tempat dan Tanggal Lahir</label>
          <input type="text" name="tgl" id="tgl" onkeyup="validateTanggal()">
          <span id="tgl-error"></span>
      </div>
      <div class="input-control">
          <label>Alamat </label>
          <textarea rows="3" name="alamat" id="alamat" onkeyup="validateAlamat()" ></textarea>
          <span id="alamat-error"></span>     
      </div>
      <div class="input-control">
        <label>Status</label>
        <input type="text" name="stat" id="stat" onkeyup="validateStatus()">
        <span id="stat-error"></span>
      </div>

        <button onclick="return validateForm()" name="Input">Submit</button><button type="reset">Ulang</button>
        <span id="submit-error"></span>

      </form>
    </div>

    <script src="responsipweb.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/typed.js@2.0.11"></script>
</body>
</html>
