var namaaError = document.getElementById('namaa-error');
var namaError = document.getElementById('nama-error');
var tglError = document.getElementById('tgl-error');
var alamatError = document.getElementById('alamat-error');
var statError = document.getElementById('stat-error');
var submitError = document.getElementById('submit-error');


function validateNamea(){
	var namaa = document.getElementById('namaa').value;

	if(namaa.length == 0){
		namaaError.innerHTML = 'Wajib diisi!';
		return false;
	}
	namaaError.innerHTML = '<i class="fas fa-check-circle"></i>';
	return true;
}
function validateName(){
	var nama = document.getElementById('nama').value;

	if(nama.length == 0){
		namaError.innerHTML = 'Wajib diisi!';
		return false;
	}
	namaError.innerHTML = '<i class="fas fa-check-circle"></i>';
	return true;
}
function validateAlamat() {
	var alamat = document.getElementById('alamat').value;
	var required = 20;
	var left = required - alamat.length;

	if(left>0){
		alamatError.innerHTML = left + '  Wajib diisi!';
		return false;
	}
	alamatError.innerHTML = '<i class="fas fa-check-circle"></i>';
	return true;
}
function validateTempat(){
	var tgl = document.getElementById('tgl').value;

	if(tglError.length == 0){
		tglError.innerHTML = 'Wajib diisi!';
		return false;
	}
	tglError.innerHTML = '<i class="fas fa-check-circle"></i>';
	return true;
}
function validateStatus(){
	var stat = document.getElementById('stat').value;

	if(statError.length == 0){
		statError.innerHTML = 'Wajib diisi!';
		return false;
	}
	statError.innerHTML = '<i class="fas fa-check-circle"></i>';
	return true;
}

function validateForm(){
	var namaa = document.getElementById('namaa').value;
	var nama = document.getElementById('nama').value;
	var tgl = document.getElementById('tgl').value;
	var alamat = document.getElementById('alamat').value;
	var stat = document.getElementById('stat').value;

	if(!validateNamea() || !validateName() || !validateAlamat() || validateTempat() || !validateStatus()){
	}
}